<?php
require 'vendor/autoload.php';

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;
use Illuminate\Database\Capsule\Manager as Capsule;

$app = AppFactory::create();

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

$capsule = new Capsule;
$capsule->addConnection([
    'driver' => 'mysql',
    'host' => $_ENV['DB_HOST'],
    'database' => $_ENV['DB_DATABASE'],
    'username' => $_ENV['DB_USERNAME'],
    'password' => $_ENV['DB_PASSWORD'],
    'charset' => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix' => '',
]);
$capsule->setAsGlobal();
$capsule->bootEloquent();

// CORS Middleware
$app->add(function (Request $request, $handler) {
  $response = $handler->handle($request);
  
  $response = $response
      ->withHeader('Access-Control-Allow-Origin', 'https://clickquest.webdad.eu')
      ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
      ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
      ->withHeader('Access-Control-Allow-Credentials', 'true');

  if ($request->getMethod() === 'OPTIONS') {
      $response = $response
          ->withHeader('Access-Control-Allow-Origin', 'https://clickquest.webdad.eu')
          ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
          ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
          ->withHeader('Access-Control-Allow-Credentials', 'true')
          ->withStatus(200);
  }

  return $response;
});

$app->post('/register', function (Request $request, Response $response, $args) {
    $data = json_decode($request->getBody(), true);
    $name = $data['name'];
    $password = $data['password'];
    $password_hash = password_hash($password, PASSWORD_BCRYPT);

    Capsule::table('players')->insert([
        'name' => $name,
        'password_hash' => $password_hash,
        'gold' => 0,
        'xp' => 0,
        'is_busy' => false
    ]);

    $response->getBody()->write(json_encode(['message' => 'Registration successful']));
    return $response->withHeader('Content-Type', 'application/json');
});

$app->post('/login', function (Request $request, Response $response, $args) {
    $data = json_decode($request->getBody(), true);
    $name = $data['name'];
    $password = $data['password'];

    $player = Capsule::table('players')->where('name', $name)->first();

    if ($player && password_verify($password, $player->password_hash)) {
        $response = ['id' => $player->id, 'name' => $player->name, 'gold' => $player->gold, 'xp' => $player->xp];
        $response->getBody()->write(json_encode($response));
    } else {
        return $response->withStatus(401)->write(json_encode(['message' => 'Invalid credentials']));
    }

    return $response->withHeader('Content-Type', 'application/json');
});

$app->post('/players', function (Request $request, Response $response, $args) {
    $data = $request->getParsedBody();
    $player = Capsule::table('players')->insertGetId(['name' => $data['name'], 'gold' => 0, 'xp' => 0, 'is_busy' => false]);

    $response->getBody()->write(json_encode(['id' => $player, 'name' => $data['name'], 'gold' => 0, 'xp' => 0]));
    return $response->withHeader('Content-Type', 'application/json');
});

$app->post('/players/{id}/hero-stuff', function (Request $request, Response $response, $args) {
    $id = $args['id'];
    $data = $request->getParsedBody();
    $taskId = $data['task_id'];

    $player = Capsule::table('players')->find($id);
    $task = Capsule::table('tasks')->find($taskId);

    if (!$player) {
        return $response->withStatus(404)->write('Player not found');
    }

    if ($player->is_busy) {
        return $response->withStatus(400)->write('Player is already doing hero stuff');
    }

    $playerLevel = Capsule::table('levels')->where('xp_required', '<=', $player->xp)->orderBy('level', 'desc')->value('level');
    if ($playerLevel < $task->minimum_level) {
        return $response->withStatus(400)->write('Player level too low for this task');
    }

    $startTime = date('Y-m-d H:i:s');
    Capsule::table('players')->where('id', $id)->update([
        'is_busy' => true,
        'task_start_time' => $startTime,
        'current_task_id' => $taskId
    ]);

    $response->getBody()->write(json_encode(['message' => 'Hero stuff started']));
    return $response->withHeader('Content-Type', 'application/json');
});

$app->get('/highscore', function (Request $request, Response $response, $args) {
    $highscore = Capsule::table('players')->orderBy('gold', 'desc')->limit(10)->get();
    $response->getBody()->write($highscore->toJson());
    return $response->withHeader('Content-Type', 'application/json');
});

$app->get('/tasks', function (Request $request, Response $response, $args) {
    $tasks = Capsule::table('tasks')->get();
    $response->getBody()->write($tasks->toJson());
    return $response->withHeader('Content-Type', 'application/json');
});

$app->get('/players/{id}', function (Request $request, Response $response, $args) {
    $id = $args['id'];
    $player = Capsule::table('players')->find($id);

    if (!$player) {
        return $response->withStatus(404)->write('Player not found');
    }

    if ($player->is_busy) {
        $task = Capsule::table('tasks')->find($player->current_task_id);
        $startTime = strtotime($player->task_start_time);
        $currentTime = time();
        $elapsedTime = $currentTime - $startTime;

        if ($elapsedTime >= $task->time_seconds) {
            Capsule::table('players')->where('id', $id)->update([
                'gold' => $player->gold + $task->gold,
                'xp' => $player->xp + ($task->time_seconds * 2), // Example XP gain
                'is_busy' => false,
                'task_start_time' => null,
                'current_task_id' => null
            ]);
            $player = Capsule::table('players')->find($id); // Refresh player data
        } else {
            $player->remaining_time = $task->time_seconds - $elapsedTime;
        }
    }

    $response->getBody()->write(json_encode($player));
    return $response->withHeader('Content-Type', 'application/json');
});

$app->run();