<?php
require 'vendor/autoload.php';

use Illuminate\Database\Capsule\Manager as Capsule;

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

$capsule = new Capsule;
$capsule->addConnection([
    'driver' => 'mysql',
    'host' => $_ENV['DB_HOST'],
    'database' => $_ENV['DB_DATABASE'],
    'username' => $_ENV['DB_USERNAME'],
    'password' => $_ENV['DB_PASSWORD'],
    'charset' => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix' => '',
]);
$capsule->setAsGlobal();
$capsule->bootEloquent();

$players = Capsule::table('players')->where('is_busy', true)->get();

foreach ($players as $player) {
    $task = Capsule::table('hero_tasks')->find($player->current_task_id);
    $startTime = strtotime($player->task_start_time);
    $currentTime = time();
    $elapsedTime = $currentTime - $startTime;

    if ($elapsedTime >= $task->time_seconds) {
        Capsule::table('players')->where('id', $player->id)->update([
            'gold' => $player->gold + $task->gold,
            'xp' => $player->xp + ($task->time_seconds * 2), // Example XP gain
            'is_busy' => false,
            'task_start_time' => null,
            'current_task_id' => null
        ]);
    }
}